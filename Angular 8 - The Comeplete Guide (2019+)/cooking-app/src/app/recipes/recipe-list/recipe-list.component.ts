import { Recipe } from './../recipe.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Test name', 'Simply a test',
      'https://storage.needpix.com/rsynced_images/gastronomy-2760200_1280.jpg'),
    new Recipe('Test name 2', 'Simply a test 2',
        'https://storage.needpix.com/rsynced_images/gastronomy-2760200_1280.jpg')
  ];
  constructor() { }

  ngOnInit() {
  }

}
