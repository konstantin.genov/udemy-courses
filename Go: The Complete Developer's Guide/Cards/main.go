package main

import "fmt"

func main() {
	cards := loadDeckFromFile("my_deck")

	cards.shuffle()

	cards.print()

	fmt.Println(len(cards))
}
