package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

type deck []string

func newDeck() deck {
	cards := deck{}

	// iterate over the two arrays to create a new deck, we do this to avoid typing suits and values every time
	cardSuits := []string{"Spades", "Diamonds", "Clubs", "Hearts"}
	cardValues := []string{"One", "Two", "Three", "Four",
		"Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"}

	for _, suits := range cardSuits { // we tell GO to ignore a variable by placing an underscore instead of it
		for _, values := range cardValues {
			cards = append(cards, values+" of "+suits)
		}
	}
	return cards
}

func (d deck) print() {
	for _, card := range d {
		fmt.Println(card)
	}
}

func (d deck) shuffle() deck {
	rand.Seed(time.Now().UnixNano()) // add current OS time as seed value, otherwise our random generation will be the same

	for i := range d {
		// More understandable:
		// var temp string
		//temp = d[i]
		//d[i] = d[newPosition]
		//d[newPosition] = temp

		// Fancier way of doing it in one line:
		newPosition := rand.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
	return d
}

func deal(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}

func (d deck) toString() string {
	return strings.Join([]string(d), "\n")
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toString()), 0666)
}

func loadDeckFromFile(filename string) deck {
	bs, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Print("Error: ", err)
		os.Exit(1)
	}
	s := strings.Split(string(bs), "\n")
	return deck(s)
}
