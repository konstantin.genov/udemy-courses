package com.konstantingenov.Polymorphism.ObjectVsReference;

/*
Java supports polymorphism, the property of an object to take on many different forms. To
put this more precisely, a Java object may be accessed using a reference with the same type
as the object, a reference that is a superclass of the object, or a reference that defines an
interface the object implements , either directly or through a superclass. Furthermore, a cast
is not required if the object is being reassigned to a super type or interface of the object.
--
In Java, all objects are accessed by reference, so as a developer you never have direct access
to the object itself. Conceptually, though, you should consider the object as the entity that
exists in memory, allocated by the Java runtime environment. Regardless of the type of the
reference you have for the object in memory, the object itself doesn’t change
 */

public class Execute {
    public static void main(String[] args) {
        Lemur lemur = new Lemur(); // primate + hastail + lemur changes on primate
        System.out.println(lemur.age);

        HasTail hasTail = lemur;
        System.out.println(hasTail.isTailStriped());

        Primate primate = lemur;
        System.out.println(primate.hasHair());

        System.out.printf("Primate primate = lemur; object class is: %s \n", primate.getClass().toString().substring(primate.getClass().toString().indexOf("L")));
        System.out.printf("HasTail hastail = lemur; object class is: %s \n", hasTail.getClass().toString());
        if(primate instanceof Primate){
            System.out.println("primate is an instance of Primate");
        }

        if (primate instanceof Lemur){
            System.out.println("primate is an instance of Lemur");
        }


        Lemur lemur2 = (Lemur)primate;

        Object lemurAsObject = (Lemur)lemur;
        Lemur objectBackToLemur = (Lemur) lemurAsObject;
        objectBackToLemur.age = 10;

        Lemur lemur4 = new Lemur();
        Primate primate2 = (Primate) lemur4; //is this because lemur is a subclass, and needs an instance of lemur?
//        Lemur lemur2 =  (Lemur) primate2;
//        lemur2 = (Lemur)primate2;

    }
}
