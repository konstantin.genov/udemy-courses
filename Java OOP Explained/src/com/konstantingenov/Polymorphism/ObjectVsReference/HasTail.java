package com.konstantingenov.Polymorphism.ObjectVsReference;

public interface HasTail {

    public boolean isTailStriped();
}
