package com.konstantingenov.Polymorphism.ObjectVsReference;

public class Lemur extends Primate implements HasTail {
    public boolean isTailStriped() {
        return false;
    }
    public int age = 10;
}
