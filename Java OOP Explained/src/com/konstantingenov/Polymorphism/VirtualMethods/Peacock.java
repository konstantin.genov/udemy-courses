package com.konstantingenov.Polymorphism.VirtualMethods;

public class Peacock extends Bird {
    public String getName() {
        return "Peacock";
    }
}
