package com.konstantingenov.Polymorphism.VirtualMethods;

public class Bird {
    public String getName() {
        return "Bird";
    }

    public void displayInformation() {
        System.out.println("The bird name is: " + getName());
    }
}
