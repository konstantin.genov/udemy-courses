package com.konstantingenov;

import com.konstantingenov.Polymorphism.ObjectVsReference.HasTail;
import com.konstantingenov.Polymorphism.ObjectVsReference.Lemur;
import com.konstantingenov.Polymorphism.ObjectVsReference.Primate;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("This repository contains examples and explanations of the concept known as Polymorphism");
        System.out.println("If you've stumbled upon this repo, go through each package and read the notes on top of the Execute.java class");
    }
}
